#include <LiquidCrystal.h>
const int rs = 7, en = 8, d4 = 9, d5 = 10, d6 = 11, d7 = 12;
int pot_pin = A0;
int cam_pin = 13;
int but_pin = 6;
int but_val = LOW;
int but_prev = LOW;
int flag = 1;
int progress = 0;
int i;
float potval;
float intereval;
unsigned long prevMillis = 0;
LiquidCrystal lcd(7, 8, 9, 10, 11, 12);

void setup() {
  pinMode(cam_pin, OUTPUT);
  pinMode(pot_pin, INPUT);
  pinMode(but_pin, INPUT);
  lcd.begin(16, 2);
}

void loop() {
  lcd.clear();
  lcd.setCursor(1,0);
  lcd.print("Set Intereval:");
  but_val = digitalRead(but_pin);
  while(flag == 1){
    but_val = digitalRead(but_pin);
    if(but_val != but_prev){
      if(but_val == HIGH){
        flag = 2;
        lcd.clear();
      }
      but_prev = but_val;
    }
    potval = analogRead(pot_pin);
    intereval = pow((potval/90),3);
    lcd.setCursor(10,1);
    lcd.print("      ");
    lcd.setCursor(10,1);
    lcd.print(intereval);
    delay(10);
  }
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Taking Photos");
  while(flag == 2){
    but_val = digitalRead(but_pin);
      if(but_val != but_prev){
        if(but_val == HIGH){
          flag = 3;
        }
        but_prev = but_val;
      }
    unsigned long currMillis = millis();
    if(currMillis - prevMillis >= 1000*intereval){
      prevMillis = currMillis;
      lcd.setCursor(15,1);
      lcd.print("!");
      digitalWrite(cam_pin, HIGH);
      delay(500);
      digitalWrite(cam_pin, LOW);
      lcd.setCursor(0,1);
      lcd.print("                ");
    }
    progress = 16*((currMillis - prevMillis)/(1000*intereval));
    for(i=0; i<progress; i++){
      lcd.setCursor(i,1);
      lcd.print("-");
    }
  }
  while(flag == 3){
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Aborted");
    lcd.setCursor(1,1);
    lcd.print("Reset in");
    lcd.setCursor(10,3);
    lcd.print("3");
    delay(1000);
    lcd.setCursor(10,2);
    lcd.print("2");
    delay(1000);
    lcd.setCursor(10,1);
    lcd.print("1");
    delay(1000);
    flag = 1;
  }
}